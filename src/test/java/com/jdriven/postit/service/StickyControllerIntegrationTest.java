package com.jdriven.postit.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import com.jdriven.postit.PostIt;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = PostIt.class)
@WebAppConfiguration
@IntegrationTest("server.port:8181")
public class StickyControllerIntegrationTest {

    static {
        PostIt.tryHttps = false;
    }

    @Autowired
    UserService userService;

    @Test
    public void testAnonymousListing() {
        //GET: ordered stickies for anonymous
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<String> results = restTemplate.getForEntity("http://localhost:8181/sticky", String.class);
        assertEquals(200, results.getStatusCode().value());
    }

    @Test
    public void testAnonymousSearch() {
        //GET: search for stickies
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<String> results =
                restTemplate.getForEntity("http://localhost:8181/sticky/search/test", String.class);
        assertEquals(200, results.getStatusCode().value());
    }

    @Test(expected = HttpClientErrorException.class)
    public void testAnonymousPost_Forbidden() {
        //POST: anonymous posted sticky
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> forbiddenSticky = new HttpEntity<String>("{\"content\":\"anonymous post\"}", headers);
        restTemplate.postForLocation("http://localhost:8181/sticky", forbiddenSticky);
    }

    @Test
    public void testBehavioralResults() {
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        
        // TODO GET: cookie and matching csrf token
        //HttpHeaders csrfHeaders = new RestTemplate().getForEntity("http://localhost:8181/csrf", String.class).getHeaders();
        //headers.set("Cookie", csrfHeaders.get("Set-Cookie").get(0).substring(0, 43));
        //headers.set("X-CSRF-TOKEN", csrfHeaders.get("Set-Cookie").get(1).substring(11));

        //POST: authenticate 
        HttpEntity<String> auth = new HttpEntity<String>("{\"username\":\"happy\",\"password\":\"happy\"}", headers);
        HttpHeaders responseHeaders = restTemplate.postForEntity("http://localhost:8181/login", auth, Void.class)
                .getHeaders();
        if (responseHeaders.get("Set-Cookie") != null) {
            String newCookieString = responseHeaders.get("Set-Cookie").get(0).substring(0, 43);
            final List<String> cookies = headers.get("Cookie");
            if (cookies != null) {
                for (String cookie : cookies) {
                    newCookieString += "; " + cookie;
                }
            }
            headers.set("Cookie", newCookieString);
        }

        //POST: sticky 1
        HttpEntity<String> newSticky1 = new HttpEntity<String>(
                "{\"content\":\"myspace owned by samy\", \"template\":\"templates/normal\", \"color\":\"yellow\"}",
                headers);
        assertTrue(restTemplate.postForLocation("http://localhost:8181/sticky", newSticky1)
                .toString().contains("/sticky/20"));

        //POST: sticky 2
        HttpEntity<String> newSticky2 = new HttpEntity<String>(
                "{\"content\":\"heartbleed exposes your private key\", \"template\":\"templates/normal\", \"color\":\"yellow\"}",
                headers);
        assertTrue(restTemplate.postForLocation("http://localhost:8181/sticky", newSticky2)
                .toString().contains("/sticky/21"));

        //GET: ordered stickies
        ResponseEntity<String> listing1 = restTemplate.getForEntity("http://localhost:8181/sticky", String.class);
        assertEquals(200, listing1.getStatusCode().value());
        assertTrue(listing1.getBody().contains("samy"));
        assertTrue(listing1.getBody().contains("heartbleed"));
        assertTrue(listing1.getBody().indexOf("heartbleed") < listing1.getBody().indexOf("samy")); //latest first

        //GET: search for stickies
        ResponseEntity<String> search =
                restTemplate.getForEntity("http://localhost:8181/sticky/search/sam", String.class);
        assertEquals(200, search.getStatusCode().value());
        assertTrue(search.getBody().contains("samy"));

        //GET: ordered based on previous search behavior
        ResponseEntity<String> listing2 = restTemplate.getForEntity("http://localhost:8181/sticky", String.class);
        assertEquals(200, listing1.getStatusCode().value());
        assertTrue(listing2.getBody().contains("samy"));
        assertTrue(listing2.getBody().contains("heartbleed"));
        assertTrue(listing2.getBody().indexOf("heartbleed") > listing2.getBody().indexOf("samy")); //most relevant first
    }
}
