var app = angular.module('postItApp', []);

app.controller('StickyCtrl', function ($scope, $http) {

	$scope.authenticated = false;
	$scope.header = undefined;
	$scope.content = 'Mijn eerste sticky post!';
	$scope.username = 'happy';
	$scope.password = 'happy';
	$scope.phrase = '';
	$scope.stickies = [];
	$scope.template = {name: 'text only', url: 'templates/normal'};
	$scope.templates = [{name: 'text only', url: 'templates/normal'}, {name: 'with image', url: 'templates/image'}];
	$scope.templatesByName = $scope.templates.reduce(function(byName, t){ byName[t.name] = t.url; return byName}, {});
	$scope.color = 'yellow';
	$scope.colors = ['yellow','pink','blue'];
	
	$http.get('/user/current').success(function(user) {
		if(user){
			$scope.authenticated = true;
			$scope.username = user.username;			
		}
	});

	// TODO CSRF token ophalen
	// $http.get('/csrf'); //plaatst het token in cookie
    // $http.defaults.xsrfHeaderName = 'X-CSRF-TOKEN';
    // $http.defaults.xsrfCookieName = 'CSRF-TOKEN';

	$scope.login = function () {
		$http.post('/login', { username: $scope.username, password: $scope.password }).success(function () {
			$scope.authenticated = true;
        });
	};
	
	$scope.logout = function () {
		$http.post('/logout').success(function () {
			$scope.authenticated = false;
			$http.get('/csrf');
        });
	};

    $scope.postIt = function () {
        $http.post('/sticky', { header: $scope.header, content: $scope.content, color: $scope.color, template: $scope.template.url }).success(function () {
			$scope.initBoard();
        });
    };
	
	$scope.initBoard = function () {	
		$http.get('/sticky').success(function (data) {
			$scope.stickies = data;
		});
	};
	
	$scope.setColor = function (c) {
		$scope.color = c;
	};
	
	$scope.setTemplate = function (t) {
		$scope.template = t;
		if($scope.template.url == 'templates/image') {
			$scope.header = $scope.content;
			$scope.content = "http://localhost:8080/img/smiley.png";
		} else {
			$scope.content = $scope.header;
			$scope.header = undefined;
		}	
	};
	
	$scope.search = function (phrase) {
		$http.get('/sticky/search/' + $scope.phrase).success(function (data){
			$scope.stickies = data;
		});
	}
});
app.directive('sticky', function () {
    var init = function(scope, element, attrs) {
		if(scope.content.header)
    		element.html('<div class="sticky ' + scope.content.color + '"><div><h4>' + scope.content.header + '</h4><img src="'+ scope.content.content +'"><span class="signed">' + scope.content.username + '</span></div></div>');
    	else
    		element.html('<div class="sticky ' + scope.content.color + '"><div>' + scope.content.content + '<span class="signed">' + scope.content.username + '</span></div></div>');
    }

    return {
        restrict: "E",
        replace: true,
        link: init,
        scope: {
            content:'='
        }
    };
});