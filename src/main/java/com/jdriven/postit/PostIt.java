package com.jdriven.postit;

import java.io.File;

import javax.servlet.Filter;

import org.apache.catalina.connector.Connector;
import org.apache.coyote.http11.Http11NioProtocol;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.embedded.EmbeddedServletContainerFactory;
import org.springframework.boot.context.embedded.tomcat.TomcatConnectorCustomizer;
import org.springframework.boot.context.embedded.tomcat.TomcatEmbeddedServletContainerFactory;
import org.springframework.boot.context.web.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.filter.CharacterEncodingFilter;

import com.jdriven.postit.model.User;
import com.jdriven.postit.repo.UserRepository;
import com.jdriven.postit.security.PostItSecurity;

@EnableAutoConfiguration
@Configuration
@ComponentScan
public class PostIt extends SpringBootServletInitializer {

    public static boolean tryHttps = true;

    public static void main(String[] args) {
        SpringApplication.run(PostIt.class, args);
    }

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(PostIt.class);
    }

    @Bean
    public InitializingBean insertDefaultUsers() {
        return new InitializingBean() {
            @Autowired
            private UserRepository userRepository;

            public void afterPropertiesSet() throws Exception {
                addUser("happy", "happy");
                addUser("evil", "evil");
            }

            private void addUser(String username, String password) {
                User user = new User();
                user.setUsername(username);
                user.setPassword(password);
                userRepository.save(user);
            }
        };
    }

    @Bean
    public Filter characterEncodingFilter() {
        CharacterEncodingFilter characterEncodingFilter = new CharacterEncodingFilter();
        characterEncodingFilter.setEncoding("UTF-8");
        characterEncodingFilter.setForceEncoding(true);
        return characterEncodingFilter;
    }

    @Bean
    public PostItSecurity applicationSecurity() {
        return new PostItSecurity();
    }

    @Bean
    public EmbeddedServletContainerFactory servletContainer() {
        TomcatEmbeddedServletContainerFactory factory = new TomcatEmbeddedServletContainerFactory();

        final String keystoreFilename = getKeyStoreFilename();
        if (keystoreFilename != null && tryHttps) {
            factory.addConnectorCustomizers(new TomcatConnectorCustomizer() {
                @Override
                public void customize(Connector connector) {
                    connector.setScheme("https");
                    connector.setSecure(true);

                    Http11NioProtocol proto = (Http11NioProtocol) connector.getProtocolHandler();
                    proto.setSSLEnabled(true);

                    proto.setKeystoreFile(keystoreFilename);
                    proto.setKeystorePass("jdriven");
                    proto.setKeystoreType("JKS");
                    proto.setKeyAlias("mykey");
                }
            });
            return factory;
        }

        return factory;
    }

    private String getKeyStoreFilename() {
        String path = getClass().getResource("/").getPath();
        final String filename = "postit.keystore";
        for (int i = 0; i < 5; i++) {
            if (new File(path + filename).exists()) {
                return path + filename;
            }
            path += "../";
        }
        return null;
    }
}
