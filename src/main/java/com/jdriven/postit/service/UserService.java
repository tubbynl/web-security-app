package com.jdriven.postit.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jdriven.postit.model.User;
import com.jdriven.postit.repo.UserRepository;

@Service
public class UserService extends AbstractService<User> {

    @Autowired
    private UserRepository userRepo;

    @Override
    protected UserRepository getRepository() {
        return userRepo;
    }

    public User findByUsername(String username) {
        return userRepo.findByUsername(username);
    }
}
