package com.jdriven.postit.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.jdriven.postit.model.AbstractModel;

public abstract class AbstractService<T extends AbstractModel> {

    @Transactional
    public T save(T model) {
        return getRepository().save(model);
    }

    @Transactional
    public boolean remove(Long id) {
        final T model = getRepository().findOne(id);
        if (model != null) {
            getRepository().delete(id);
            return true;
        }
        return false;
    }

    public T findById(Long id) {
        return getRepository().findOne(id);
    }

    public List<T> findAll() {
        return getRepository().findAll();
    }

    protected abstract JpaRepository<T, Long> getRepository();
}
