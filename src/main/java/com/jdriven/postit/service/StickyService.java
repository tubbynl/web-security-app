package com.jdriven.postit.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import com.jdriven.postit.model.Search;
import com.jdriven.postit.model.Sticky;
import com.jdriven.postit.model.User;
import com.jdriven.postit.repo.SearchRepository;
import com.jdriven.postit.repo.StickyRepository;

@Service
public class StickyService extends AbstractService<Sticky> {

    private final PageRequest LATEST_THREE = new PageRequest(0, 3, Direction.DESC, "created");
    private final PageRequest LATEST_TEN = new PageRequest(0, 10, Direction.DESC, "created");
    private final PageRequest LATEST_HUNDRED = new PageRequest(0, 100, Direction.DESC, "created");

    @Autowired
    private StickyRepository stickyRepo;

    @Autowired
    private SearchRepository searchRepo;

    @Transactional
    public List<Sticky> search(Search search) {
        searchRepo.save(search);
        final String query = search.getQueryAsLike();
        return stickyRepo.findByContentLikeOrHeaderLike(query, query, LATEST_TEN).getContent();
    }

    public List<Sticky> topTen(User user) {
        final Page<Search> lastThreeSearches = searchRepo.findAll(LATEST_THREE);

        final Page<Search> lastThreeSearchesForCurrentUser = user == null ?
                null : searchRepo.findByUserId(user.getId(), LATEST_THREE);

        final Page<Sticky> lastHundredStickies = stickyRepo.findAll(LATEST_HUNDRED);

        final List<Sticky> stickies = new ArrayList<Sticky>(10);
        final List<RelevantSticky> relevantStickies = new ArrayList<RelevantSticky>(100);

        final int size = lastHundredStickies.getContent().size();
        for (int i = 0; i < size; i++) {
            Sticky sticky = lastHundredStickies.getContent().get(i);
            int relevance = i;

            final String uppercaseContent = sticky.getContent().toUpperCase();
            for (Search search : lastThreeSearches.getContent())
                if (uppercaseContent.contains(search.getQuery().toUpperCase()))
                    relevance -= 2;

            if (lastThreeSearchesForCurrentUser != null)
                for (Search search : lastThreeSearchesForCurrentUser.getContent())
                    if (uppercaseContent.contains(search.getQuery().toUpperCase()))
                        relevance -= 4;

            relevantStickies.add(new RelevantSticky(relevance, sticky));
        }

        //return top 10 most relevant stickies
        Collections.sort(relevantStickies);
        for (int j = 0; j < Math.min(relevantStickies.size(), 10); j++)
            stickies.add(relevantStickies.get(j).getSticky());
        return stickies;
    }

    static class RelevantSticky implements Comparable<RelevantSticky> {
        private int relevance;
        private Sticky sticky;

        RelevantSticky(int relevance, Sticky sticky) {
            this.relevance = relevance;
            this.sticky = sticky;
        }

        public Sticky getSticky() {
            return sticky;
        }

        public int compareTo(RelevantSticky rs) {
            return relevance - rs.relevance;
        }
    }

    @Override
    protected StickyRepository getRepository() {
        return stickyRepo;
    }
}
