package com.jdriven.postit.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import com.jdriven.postit.model.AbstractModel;
import com.jdriven.postit.model.User;
import com.jdriven.postit.service.AbstractService;
import com.jdriven.postit.service.UserService;

public abstract class AbstractController<T extends AbstractModel> {

    @Autowired
    private UserService userService;

    protected abstract AbstractService<T> getService();

    public ResponseEntity<Void> create(T model, UriComponentsBuilder uriBuilder) {
        if (model.getId() != null)
            throw new IllegalArgumentException("id should not be set");
        model = getService().save(model);

        UriComponents uriComponents = uriBuilder.path("/{model}/{id}")
                .buildAndExpand(model.getClass().getSimpleName().toLowerCase(), model.getId());
        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(uriComponents.toUri());
        return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
    }

	public List<T> list() {
        return getService().findAll();
    }

    public T show(final Long id) {
        return getService().findById(id);
    }

    @ExceptionHandler
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public String handleNotFound(IllegalStateException e) {
        return e.getMessage();
    }

    @ExceptionHandler
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public String handleExceptions(Exception e) {
        if (e instanceof MethodArgumentNotValidException) {
            String errorMessage = ((MethodArgumentNotValidException) e).getBindingResult().getFieldError().toString();
            final int index = errorMessage.indexOf(';');
            return index != -1 ? errorMessage.substring(0, index) : errorMessage;
        }
        return e.getMessage();
    }

    protected final User getCurrentUser() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth.isAuthenticated() && auth.getPrincipal() instanceof UserDetails) {
            final String username = ((UserDetails) auth.getPrincipal()).getUsername();
            return userService.findByUsername(username);
        }
        return null;
    }

    protected final boolean isAuthenticated() {
        return SecurityContextHolder.getContext().getAuthentication().isAuthenticated();
    }
}
