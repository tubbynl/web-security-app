package com.jdriven.postit.controller;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import com.jdriven.postit.model.Search;
import com.jdriven.postit.model.Sticky;
import com.jdriven.postit.service.AbstractService;
import com.jdriven.postit.service.StickyService;

@RestController
@RequestMapping("/sticky")
public class StickyController extends AbstractController<Sticky> {
    
    @Autowired
    private StickyService service;

    @Override
    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<Void> create(@RequestBody @Valid Sticky sticky, UriComponentsBuilder uriBuilder) {
        sticky.setUser(getCurrentUser());

        // TODO: validateImageUrl(sticky);

        return super.create(sticky, uriBuilder);
    }

    @Override
    @RequestMapping(method = RequestMethod.GET)
	public List<Sticky> list(){
		return service.topTen(getCurrentUser());
    }

    @Override
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public Sticky show(@PathVariable final Long id) {
        return super.show(id);
    }

    @RequestMapping(value = "/search/{query}", method = RequestMethod.GET)
    public List<Sticky> search(@PathVariable final String query) {
        final Search search = new Search();
        search.setUser(getCurrentUser());
        search.setQuery(query);
        return service.search(search);
    }

    @RequestMapping(value = "new", method = RequestMethod.POST)
    public ResponseEntity<Void> createFromFormPost(@RequestParam("content") String content,
            @RequestParam("color") String color, @RequestParam("template") String template,
            @RequestParam(value = "header", required = false) String header) {

        Sticky sticky = new Sticky();
        sticky.setUser(getCurrentUser());
        sticky.setContent(content);
        sticky.setColor(color);
        sticky.setHeader(header);
        sticky.setTemplate(template);
        service.save(sticky);

        return new ResponseEntity<Void>(HttpStatus.CREATED);
    }

    @Override
    protected AbstractService<Sticky> getService() {
        return service;
    }

    @SuppressWarnings("unused")
    private void validateImageUrl(Sticky sticky) {
        if (sticky.getTemplate().endsWith("image")) {
            try {
                final URI uri = new URI(sticky.getContent());
                final String path = uri.getPath();
                if (!(path.endsWith(".png") || path.endsWith(".jpg") || path.endsWith(".gif"))) {
                    throw new IllegalArgumentException("Invalid image URL: " + sticky.getContent());
                }
            } catch (URISyntaxException e) {
                throw new IllegalArgumentException("Invalid image URL: " + e.getMessage());
            }
        }
    }
}
