package com.jdriven.postit.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.jdriven.postit.model.User;
import com.jdriven.postit.service.AbstractService;
import com.jdriven.postit.service.UserService;

@RestController
@RequestMapping("/user")
public class UserController extends AbstractController<User> {
    
    @Autowired
    private UserService service;

    @Override
    protected AbstractService<User> getService() {
        return service;
    }

    @RequestMapping(value = "/current", method = RequestMethod.GET)
    public User current() {
        User user = getCurrentUser();
        if (user != null) {
            user.setPassword(null);
        }
        return user;
    }
}
