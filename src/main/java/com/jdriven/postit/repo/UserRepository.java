package com.jdriven.postit.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.jdriven.postit.model.User;

public interface UserRepository extends JpaRepository<User, Long> {

    User findByUsername(String username);

}
