package com.jdriven.postit.repo;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import com.jdriven.postit.model.Search;

public interface SearchRepository extends JpaRepository<Search, Long> {

    Page<Search> findByUserId(Long userId, Pageable pageable);
}
