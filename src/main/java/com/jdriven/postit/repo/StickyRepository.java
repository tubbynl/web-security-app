package com.jdriven.postit.repo;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import com.jdriven.postit.model.Sticky;

public interface StickyRepository extends JpaRepository<Sticky, Long> {
    Page<Sticky> findByContentLikeOrHeaderLike(String contentQuery, String headerQuery, Pageable pageable);
}