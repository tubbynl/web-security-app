package com.jdriven.postit.model;

import java.util.Date;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@MappedSuperclass
public abstract class AbstractModel {

    @Temporal(TemporalType.TIMESTAMP)
    private Date created;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getCreated() {
        return created;
    }

    @PrePersist
    private void prePersist() {
        if (created == null)
            created = new Date();
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + ": " + getId();
    }
}
