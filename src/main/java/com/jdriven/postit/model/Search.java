package com.jdriven.postit.model;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
public class Search extends AbstractModel {

    @NotNull
    @Size(min = 3, max = 50)
    private String query;

    @ManyToOne(optional = true)
    private User user;

    public String getQuery() {
        return query;
    }

    public String getQueryAsLike() {
        return "%" + query + "%";
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
