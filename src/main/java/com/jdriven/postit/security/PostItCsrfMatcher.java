package com.jdriven.postit.security;

import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;

import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.security.web.util.matcher.RequestMatcher;

final class PostItCsrfMatcher implements RequestMatcher {
    private Pattern allowedMethods = Pattern.compile("^(GET|HEAD|TRACE|OPTIONS)$");
    private RequestMatcher cspMatcher = new AntPathRequestMatcher("/csp");

    public boolean matches(HttpServletRequest request) {
        return !allowedMethods.matcher(request.getMethod()).matches() && !cspMatcher.matches(request);
    }
}