package com.jdriven.postit.security;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.web.csrf.CsrfToken;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/csrf")
public class CsrfCookieController {

	@RequestMapping(method = RequestMethod.GET)
	public void create(HttpServletRequest request, HttpServletResponse response) {
		CsrfToken token = (CsrfToken) request.getAttribute("_csrf");
		if (token != null) {
            response.addCookie(new Cookie("CSRF-TOKEN", token.getToken()));
		}
    }
}
