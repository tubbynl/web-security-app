package com.jdriven.postit.security;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/csp")
public class CspReportController {

    final Logger LOG = LoggerFactory.getLogger(CspReportController.class);

    @RequestMapping(method = RequestMethod.POST)
    public void create(@RequestBody String json) {
        LOG.info(json);
    }
}
