package com.jdriven.postit.security;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.jdriven.postit.model.User;
import com.jdriven.postit.service.UserService;

@Service
public class UserDetailsService implements org.springframework.security.core.userdetails.UserDetailsService {

    @Autowired
    private UserService userService;
    
    private final List<? extends GrantedAuthority> defaultRoles = Arrays.asList(new GrantedAuthority() {
        private static final long serialVersionUID = 1L;
        public String getAuthority() {
            return "ROLE_USER";
        }
    });

    public final UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        final User user = userService.findByUsername(username);
        if (user == null) {
            throw new UsernameNotFoundException("user not found");
        }
        return asUserDetails(user);
    }

    private final UserDetails asUserDetails(final User user) {
        return new UserDetails() {
            private static final long serialVersionUID = 1L;

            public Collection<? extends GrantedAuthority> getAuthorities() {
                return defaultRoles;
            }

            public String getPassword() {
                return user.getPassword();
            }

            public String getUsername() {
                return user.getUsername();
            }

            public boolean isAccountNonExpired() {
                return true;
            }

            public boolean isAccountNonLocked() {
                return true;
            }

            public boolean isCredentialsNonExpired() {
                return true;
            }

            public boolean isEnabled() {
                return true;
            }
        };
    }
}
