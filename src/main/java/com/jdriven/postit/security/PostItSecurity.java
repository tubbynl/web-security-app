package com.jdriven.postit.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@Order(Ordered.LOWEST_PRECEDENCE - 6)
public class PostItSecurity extends WebSecurityConfigurerAdapter {

    @Autowired
    private UserDetailsService userDetailsService;

    public PostItSecurity() {
        super(true);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
		        .exceptionHandling().and()
		        .sessionManagement().and()
		        .securityContext().and()
		        .anonymous().and() 
		        .servletApi().and()
		
		        .headers()
		        .cacheControl() //BONUS: Remove this line to let Spring add all headers considered to add security...
		        .and() 
		
		        //allow any GETs (for reading/searching by users and bots) anything else requires USER role
		        .authorizeRequests()
		        .antMatchers(HttpMethod.GET, "/**").permitAll()
		        .antMatchers(HttpMethod.OPTIONS, "/**").permitAll()
		        .anyRequest().hasRole("USER")
		
		        //custom JSON based authentication by POST of {"username":"<name>","password":"<password>"} to /login
		        .antMatchers("/login").permitAll().and()
		        .logout().logoutSuccessUrl("/").and()
		        .addFilterBefore(new JsonAuthenticationFilter("/login", authenticationManager()),
                        UsernamePasswordAuthenticationFilter.class)

                // TODO uncomment to require CSRF token
                // .csrf().and()
		;
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService);
    }

    @Override
    protected UserDetailsService userDetailsService() {
        return userDetailsService;
    }
}
